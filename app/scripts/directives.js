'use strict';

angular.module('neo4jQueryConsoleApp').directive('easyuiDatagrid', function($log) {
    return {
        // Restrict it to be an attribute in this case
        restrict: 'A',
        // responsible for registering DOM listeners as well as updating the DOM
        link: function(scope, element, attrs) {
          var modelName = attrs.easyuiDatagrid;
          scope.$watch(modelName, function(value) {
            var dom = element;
            var cols = _.map(value.columns, function(col) {
              return { field:col, title:col };
            });

            // Defer Datagrid plugin call until after the Angular
            // callstack has finished so that the DOM has been fully created
            _.defer(function() {
              $(element).datagrid({
                columns: [cols],
                onLoadSuccess: function() {
                  var merges = [];
                  var addMerge = function(merge) {
                    merges.push(merge);
                    $(element).datagrid('mergeCells',{
                      field: value.columns[merge.column],
                      index: merge.row,
                      rowspan: merge.span
                    });
                  };
                  for(var i = 0; i < value.columns.length; i++) {
                    var start = -1;
                    var current;
                    var span = 1;
                    for(var j = 0; j < value.data.length; j++) {
                      var cell = value.data[j][i];
                      if(start < 0) {
                        start = j;
                        current = cell;
                      } else {
                        if(_.isEqual(cell, current)) {
                          span++;
                        } else {
                          if(span > 1) {
                            addMerge({
                              column: i,
                              row: start,
                              span: span
                            });
                          }
                          span = 1;
                          current = cell;
                          start = j;
                        }
                      }
                    }
                    if(span > 1) {
                      addMerge({
                        column: i,
                        row: start,
                        span: span
                      });
                    }
                  }
                }
              });
            });
          });
        }
      };
  });
