'use strict';
/* jshint indent:4 */

angular.module('neo4jQueryConsoleApp').controller('MainCtrl', ['$scope', '$http', '$log', 'db', 'conf', function ($scope, $http, $log, db, conf) {

    $scope.newQuery = {};

    $scope.result = {};

    var queries = $scope.queries = [];
    var alerts = $scope.alerts = [];
    $scope.query = {};

    $scope.syncIcon = '';
    $scope.syncStatus = '';

    var neo4jServers = $scope.neo4jServers = [];

    // limit refresh rate to once per 100ms
    var showQueries = _.throttle(function (change) {
        $log.log('Something changed', change);
        db.query(function(doc) {
            if('query' == doc.type) {
                emit(doc._id, doc);
            }
        }, {reduce: false}, function(err, response) {
            $log.log('Queried ', response.rows);
            $scope.$apply(function() {
                $scope.queries = _.map(response.rows, function(row) {
                    return row.value;
                });
            });
        });
    }, 100);

    $scope.addQuery = function() {
        var newQuery = _.extend(_.clone($scope.newQuery), {
            _id: new Date().toISOString(),
            type: 'query'
        });

        $log.log('Adding query', newQuery);
        queries.push(newQuery);
        $scope.newQuery.name = '';
        $scope.newQuery.cypher = '';

        db.put(newQuery, function callback(err, result) {
            if (!err) {
                $log.log('Successfully added query ', newQuery);
            }
        });
    };

    $scope.delQuery = function(query) {
        db.remove(query);
    };

    $scope.copyQuery = function(query) {
        $scope.newQuery = {
            name: 'Copy of ' + query.name,
            cypher: query.cypher
        };
    };

    $scope.runQuery = function(query) {
        var cypher = query.cypher;
        $log.log('Running query ', cypher);
        $http.post(conf.neo4jApi + '/db/data/cypher', {
            query: cypher,
            params: {}
        }).success(function(data, status, headers, config) {
            $log.log('Received ', data);
            $('a[href="#resultsTab"]').tab('show');
            $scope.result = data;
            $scope.query = query;
        }).error(function(data, status, headers, config) {
            $log.log('Error ', data);
            $('a[href="#resultsTab"]').tab('show');
            alerts.push({
                title: data.exception,
                message: data.message
            });
        });
    };

    $scope.dismissAlert = function(alert) {
        $log.log('Dismiss alert', alert);
        var i = _.indexOf(alerts, alert);
        if(i >= 0) {
            alerts.splice(i, 1);
        }
    };

    var syncError = function() {
        $log.log('Sync error');
    };

    var updateSyncStatus = function(icon, status, digest) {
        if(!digest) {
            $scope.$apply(function() {
                $scope.syncIcon = icon;
            });
        } else {
            $scope.syncIcon = icon;
        }
        _.defer(function() {
            $('#syncStatus').tooltip('destroy');
            $('#syncStatus').tooltip({title: status});
        });
    };

    // limit refresh rate to once per 100ms
    var syncingUp = _.throttle(function(c) {
        $log.log('Syncing UP', c);
        updateSyncStatus('icon-upload', 'Upload');
        syncingFinished();
    }, 100);

    // limit refresh rate to once per 100ms
    var syncingDown = _.throttle(function(c) {
        $log.log('Syncing DOWN', c);
        updateSyncStatus('icon-download', 'Download');
        syncingFinished();
    }, 100);

    // Consider syncing finished after syncing activity stops for a second
    var syncingFinished = _.debounce(function() {
        $log.log('Syncing finished');
        updateSyncStatus('icon-time', 'Idle');
    }, 1000);

    var sync = function() {
        var remoteCouch = conf.couchDbUrl;
        $log.log('Starting replication task');
        db.replicate.to(remoteCouch, {continuous: true, complete: syncError, onChange: syncingUp});
        db.replicate.from(remoteCouch, {continuous: true, complete: syncError, onChange: syncingDown});
    };

    showQueries();

    db.info(function(err, info) {
        db.changes({
            since: info.update_seq,
            continuous: true,
            onChange: showQueries
        });
    });

    sync();

    updateSyncStatus('icon-off', 'Off', true);

}]);
