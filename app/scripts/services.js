'use strict';
/* global PouchDB */

// To have a database as a dependency that you can inject in a service
angular.module('neo4jQueryConsoleApp').factory('db', function() {
  return new PouchDB('neo4jqueries');
});

angular.module('neo4jQueryConsoleApp').factory('conf', function() {
  return {
    neo4jApi: 'http://172.18.15.41:7474',
    couchDbUrl: 'http://172.18.15.28:5984/neo4j-queries'
  };
});
